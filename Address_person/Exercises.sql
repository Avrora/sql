USE first;

-- Exercises
-- 1. All addresses where city is Lyon
-- SELECT * FROM address WHERE city = "Lyon";
-- 
-- 2. All addresses where person>29 years
-- SELECT * FROM address LEFT JOIN person ON person.id = address.id_person WHERE age > 29;

-- 3. Address vacant in Lyon
-- SELECT * FROM address WHERE id_person IS NULL AND address.city = "Lyon";